# TP3

## EX1 
<pre><code>echo 'bienvenue taper 4mots'
read a b c d
echo $d $c $b $a
</code></pre>
## EX2
<pre><code>echo 'bienvenue voici 4 mots a l'envers:'
echo $4 $3 $2 $1 
</code></pre>
## EX3
<pre><code>for i in 5 4 3 2 1 decollage do
    echo $i
done
</code></pre>
## EX5
<pre><code>for i in 1 2 3 4 5 do
    touch fich${i}.txt
    cp$1/fich${i}.txt
done
</code></pre>
## EX6 
<pre><code>for nom in 'ls $1'
do
    echo .:a$nom
done
</code></pre>

## EX7
<pre><code>if(grep $1 annuaire.txt
    then echo"utilisateur trouvé!"
else 
    echo "utilisateur non trouvé..."
fi
</code></pre>

## EX8
<pre><code> 
case $1 in
0)echo zero;;
1)echo one;;
2)echo two;;
3)echo three;;
4)echo four;;
5)echo five;;
*)echo Inconnu!;;
esac
</code></pre>

## EX9


## EX10 

<pre><code> 
for nom in 'ls/urs/include/*.h'
do 
    compte=`wc -l $nom|cut -fl -d""`
    if(test$compte -gt 100)
    then 
        echo $nom
        fi
done
</code></pre>

## EX11 
<pre><code>
for nom in `ls $3/*.$1`
do 
    nom_sans_ext=`basename $nom $1
    mv $nom $nom_sans_ext.$2
done
</code></pre>