# Les structures
## Structure for :
 <pre><code> For nom in liste
 Do
 Commandes
 Done </code></pre> 
* La liste est une suite de valeurs séparées par des espaces
* SI aucune liste n’est fournie dans le code du script, celle-ci est constituée par la liste des arguments

## Structure if :
 <pre><code> if commande1
 Them commande2
 Else Commande3
 Fi </code></pre> 
* La partie else est optionnelle
* la commande 1 est évaluée par rapport à son code de retour

### If inbriquée
<pre><code> if commande1
 Them commande2
 Elif commande3
 Then commande4
 Elif Commande5
 ... </code></pre> 
 
#### Et
* commande1&&commande2
* <==>if commande1 then commande2 fi

#### Ou
* cmd1||cm2 la commande2 sera exécutée uniquement si la commande1 se termine sur un code d'erreur

## Structure Case
<pre><code> case chaine in 
 motif1) Them commande1
 ...
 motifn) commanden
 Esac
 </code></pre> 
 * un motif peut être une expression régulière
 * le symbole "**|**" permet de simuler le "ou" pour un motif

## While

<pre><code> while commande1
 Do commande2
 Done
 </code></pre>
 
## Until

<pre><code> Util commande1
 Do commande2
 Done
 </code></pre> 
 
## Test
<pre><code>Test Expression  
[Expression]  
</code></pre>
* Test -w fichier vrai si fichier existe et est autorisé en écriture
* Test -r fichier idem en lecture
* Test -x fichier idem en execution
* Test -d fichier vrai si fichier existe et est un répertoire
* Test -f fichier vrai si fichier existe et n'estpas un répertoire
* Test -s fichier vrai si fichier existe et n'a pas une taille nulle
* Test -z vrai si la chaine s1 est vide 
* Test -n test inverse du précédent
* Test s1=s2 vrai si les chaines s1 et s2 sont identique 
* Test s1!=s2 vrai si les chaines s1 et s2 sont differentes
* Test n1 -ne n2 vrai si les nombres n1 et n2 ne sont pas egaux
* Test n1 -eq n2 test inverse
* Test n1 -gt n2 vrai si n1>n2
* Test n1 -lt n2 vrai si n1<n2
* Test n1 -ge n2
* Test n1 -le n2

## Expr

* Expr e1 + e2
* Expr e1-e2
* Expr e1-e2
* Expr e1/e2
* Expr e1%e2

* != negation -a **ET** Logique -o **OU** Logique pour regrouper plusieurs expressions 
* La priorité de -a est supérieur à celle de -o
* Chaque primituve et opérande doit etre séparée par un blanc 
* Les parenthèses doivent être protégée ("\" ou "**`**")pour éviter d'être interprétées par le Shell