# Droit d'acces 

## Unix est multi-utilisateur :
	Plusieurs utilisateurs peuvent utiliser simultanément la machine  
	Les fichiers sont propres à chaque utilisateur  
### Chaque utilisateur a :  
	Un login identifié au niveau du noyau par un UID  
	Un groupe identifié au niveau du noyau par un GID   
	Utilisateur particulier : root (ou super utilisateur)  
## Gestion des droits :
### Pour gérer l’aspect multi-utilisateur, les fichiers UNIX ont des droits : Lecture, Ecriture, Exécution  
	Les droits sont différents pours : L’utilisateur, Le groupe, Les autres  
	Pour un répertoire : le droit d’exécution est un droit d’accès au répertoire  
	Un fichier texte avec les droits d’exécution devient un script  
### Chmod : modifier les droits d’in fichier ou d’un répertoire :  
	Chmod utilisateur opération droits d’accès fichier  
* Utilisateur u,g,o   
* Opération +/-  
* Droit d’accès : r,w,x  
* Ex : comment pour un fichier changer les droits du propriétaire et du groupe en lecture seulement ? Réponse : chmod ug+r-wx fichier
### Chmod code_octal fichier
*Le code octal est calculé sur 3*3 bits représentantes droits d’accès pour le propriétaire, le groupe et les autres. 1 correspond à x, 2 à w et 4 à r 
* Ex(précèdent) : chmod 440 fichier  
### Chown/chgrp modifier le propriétaire/groupe d’un fichier   
	Chown nouveau_proprietaire fichier   
	Chgrp nouveau_groupe fichier  

# Les Descripteurs 
Un accès à une entrée/sortie quelconque est nommé descripteur  
Sous UNIX, un descripteur est un fichier   
## Descripteur particuliers : entrée et sortie standard :   
* Fournis par le système 
* Permettent de lire des données au clavier et d’en écrire à l’écran
* Entrée standard stdin
* Sortie standard stdout
* L’entrée et la sortie standard peuvent être redirigées 
## Sortie erreur stderr
« > » Sert à rediriger la sortie standard  
Ex : ls > log_ls.txt redirige la sortie de ls dans le fichier log_ls.txt   
« < » Sert à rediriger l’entrée standard  
Ex : read<read_data.txt redirige ‘entrée standard de read vers le fichier read_data.txt   
« >& » sert à rediriger à la fois la sortie standard et la sortie erreur   
« >> » et « << » permettent d’ajouter à la fin d’in fichier existant au lieu de l’écraser   
