# Architecture

***************************

## Dates

### Première époque (1946-1955):
*1946* > Premier ordinateur **ENIAC**  
*1947* > Invention du Transistor  

### Seconde époque (1955-1970):
*1958* > Premier jeu vidéo  
*1966* > Première console de jeu vidéo  

### Troisième époque (1971-1980):
*1976*  > Fondation d'Apple  
*1977* > Nouvelle gamme de calculatrices de poche par Texas Instruments  

### Quatrième époque (1980-1990):
*1981* > **213** machine connectées sur internet  
*24 janvier 1984* > Steve Jobs présente **l'Apple Machintosh** au public  

### Cinquième époque(1990-2000):
*1989* > **100.000** ordinateurs connectés a internet  
*1997* > L'Intelligence artificielle avec Deep Blue  

### Sixième époque(2000-2010):
*2001* > Wikipédia  
*2004* > Facebook  

### Septième époque(2010-?):
*2010* > **300 millions** de ventes de smartphones  
*2016* > **1,5 milliards** de ventes de smartphones  

************************

## Representation des données

### Base 2 sur 8 bits

On represente de 0 à 255
 * Ex: 
 * 79-64=15
 * 15-8=7
 * 7-4=3
 * 3-2=1
 * 1-1=0

> Ce qui nous donne 0100 1111

### Base 2 en base 10

* Ex: 1010 1110 
* 128+32+8+4+2

> Ce qui nous donne 174

### Complement à deux

On veut representé X

Si x ** < ** 1 Le premier bit est à 1  
Les 7 autres bits représente 128+X en base 2 sur 7bits  

Si x ** >= ** 1 Le premier bit est à 0   
Les 7 autres bits représente X en base 2 sur 7bits   

* Ex: 
* **-25**<0
* 128**-25**=103
* 103-64=39
* 39-32=7
* 7-4=3
* 3-2=1
* 1-1=0

> Ce qui nous donne 1110 0111

### Conversion complement à deux vers Base 10

Si Le premier bit est à 1   
Le premier bit(gauche)  vaut **-128**  
Les autres bits ont des poids positifs de 1 à 64  

* Ex: 
* 1101 1011
* **-128** +64+16+8+2+1

>  Ce qui nous donne -37

### Hexadecimal 

On découpe par tranche de 4 bits  
Chaque tranche représente un caractère   

* Ex: 
* 1101 1110 0101 1011 

>  D    E    5     B

*  A    F    8     B

> 1010 1111 1000 1011

### IEEE-754 
On veut representé X  

Si  
x **=**0 alors X est representé par 32 bits a 0  

Sinon  
X=(-1)^s*2^e(1+m)
*s=signe; e=exposant; m=mantisse*   

#### Trouver le signe 

Si X<0 **S=1**  
Si X>=0 **S=0**  

#### Trouvez e et m 

##### Premier cas X>=2
On divise |X| / 2 jusqu'à ce que X soit dans l'intervalle[1;2[  
e=**+** nombre de division   
m= derniere valeur **-1**   

* Ex: -8,8
* S=1
* 8,8/2=4,4
* 4,4/2=2,2
* 2,2/2=1,1

> S=1 e=3 et m=1,1**-**1= 0,1

##### Deuxieme cas |X|<1
On multiplie |X| par 2 jusqu'à ce que X soit dans l'intervalle[1;2[  
e=**-** nombre de division   
m= derniere valeur **-1**   

* Ex: 0,325
* S=0
* 0,325*2=0,65
* 0,65*2=1,3

> S=0 e=-2 et m=1,3**-**1= 0,3

##### Troisieme cas 1<=|X|<2

e=0  
m= |X| **-1**   

* Ex: 1,4

> S=0 e=0 et m=1,4**-**1= 0,4

#### Trouver la suite de 32bits
3 champs: signe 1bits, exposant 8bits et mantisse 23bits  
Exposant se calcul : **E=127+e** on le represente en base 2 sur 8bits  

En suite on multiplie la mantisse par 2 jusqu'à retrouver la meme valeur qu'en debut de periode ou l'on fera **-1**  

* Ex: X=-8,8
* S=1 e=3 et m=1,1**-**1= 0,1
* E=127+3= 130 = 1000 0010
* 0,1*2=0,2=0+0,2 
* 0,2*2=0,4=0+0,4 <em>Début de période</em>
* 0,4*2=0,8=0+0,8
* 0,8*2=1,6=1+0,6
* 0,6*2=1,2=1+0,2  <em>Fin de période</em>
* 0,2*2=0,4=0+0,4

> 1 1000 0010 0 0011 0011 0011 0011 0011 00 
> On doit avoir 23 bit de mantisse donc on rajoute le debut de la mantisse pour les avoirs.

#### Ecrire le resultat en Hexa 

* Ex: X=-8,8
* 1 1000 0010 0 0011 0011 0011 0011 0011 00 

> C10C CCCC

### Retrouver X 
On part d'une Valeur Hexadecimale.  

#### Forme binaire 
On l'ecrit sous forme binaire  

* Ex:
* C0F0 0000
* **1** *100 0000 1* 111 0000 0000 0000 0000 0000
* s=1 (caractère en gras)
* exposant= 100 000 1 (italique)
* mantisse=111 0000 0000 0000 0000 0000

#### Retrouver l'exposant 
Si E=127 E etant la valeur de l'exposant +e alors **e=E-127**  

* Ex:
* Exposant= 1000001
* E= 128+1=129
* e=E-127=2 

#### La mantisse
Le premier bit de la mantisse a comme poids 2^-1 le deuxième 2^-2 puis 2^-3 et le dernier 2^-23  

On fait la somme des poids des bits à 1  
    
* Ex:
* Mantisse 11100000000000000000000
* M=1/2+1/4+1/8=7/8

#### Retrouver X
X=(-1)^s * 2^e * (1+m)  

* Ex 
* X=(-1)^1 * 2^2 *(1+7/8)
* =-15/2

> Le résultat doit être donné sous la forme d'une fraction irréductible

***************************

<h2> Codes détécteurs et correcteurs d'erreurs </h2>

#### Bit de parité 
Ajout d'un bit, pour avoir un nombre de bits paire  

* Ex: 
* 1100 1111 0 = Non erronée
* 1111 1110 0 =Erronée

### Codage de Hamming 
Si la donnée N=8 bits la donnée finale sera sur 12bits  
Si la donnée N=12 la donnée finale sera sur 17bits  
* F17 **F16** F15 F14 F13 F12 F11 F10 F09 **F08** F07 F06 F05 **F04** F03 **F02 F01** Seront les bits de controle. 

> Les puissance de 2 sont les bits de contrôle 

On construit les ensembles de bit E1, E2, E3... En fonction du nombre de bit  

* E1={F1 F3 F5 F7 F9 F11 [...]}
* E2={F2 F3 F6 F7 F10 F11 [...]}
* E3={F4 F5 F6 F7 F12[...]}
* E4={F8 F9 F10 F11 F12[...]}

> Pour crée un ensemble on prend dans un tableau de bits lors que la donnée d'une colonne est à 1: 0001[1]; 0011[3]; 0101[5]...

On choisit le bit de controle pour que dans chaque ensemble **le nombre de 1 soit pair**  

* Ex:  F12 F11 F10 F09 **F08** F07 F06 F05 **F04** F03 **F02 F01**
*       1   1   0   0     1   1   0     1              
* E1={F1 F3 F5 F7 F9 F11}={F1 1 0 1 0 1}= F1=1
* E2={F2 F3 F6 F7 F10 F11}={F2 1 1 1 0 1}=F2=0
* E3={F4 F5 F6 F7 F12}={F4 0 1 1 1}=F4=1
* E4={F8 F9 F10 F11 F12}={F8 0 0 1 1}=F8=0 

> F12 F11 F10 F09 **F08** F07 F06 F05 **F04** F03 **F02 F01**
>  1   1   0   0     0     1   1   0     1     1     0   1      

#### Correction codage de Hamming

On construit les ensembles: E1, E2... *les bits de controle étant déjà dans la donnée*  
Si le nombre de bit à 1 est paire **e=0**  
Sinon **e=1**  
Pour qu'une donnée ne soit pas corrompue tous *e valent 0*  
Si la donnée est corrompue on inverse la valeur que les **e** donnent   

* Ex:  F12 F11 F10 F09 **F08** F07 F06 F05 **F04** F03 **F02 F01**
*       1   0   0   0  0   1   1  0  1  1  0 1            
* E1={F1 F3 F5 F7 F9 F11}={1 1 0 1 0 0}= **e1=1**
* E2={F2 F3 F6 F7 F10 F11}={0 1 1 1 0 0}=**e2=1**
* E3={F4 F5 F6 F7 F12}={1 0 1 1 1}=**e4=0**
* E4={F8 F9 F10 F11 F12}={0 0 0 1 0}=**e8=1** 
* La donnée est corrompue e=(1011)=11 on inverse la valeur de F11

> F12 F11 F10 F09 **F08** F07 F06 F05 **F04** F03 **F02 F01**>
>   1   **1**   0   0  0  1    1   0   1   1    0     1      

### CRC 
On prend la donnée, que l'on multiplie par X^4, on la divise par le polynome générateur, **le reste = est la valeur du CRC** que l'on ecrit à la fin de la donnée  

* Ex: 1001 0011
* X^7+X^4+X+1
* J'ai pas trouver d'image assez claire du coup cf cours


#### Verifier le CRC

On prend la donnée, que l'on ne multiplie pas, on la divise par le polynome générateur, si **le reste = 0** alors la donnée est pas corrompue  

* Ex: 1001 0111 1001
* X^11+X^8+X^6+X^5+X^4+X^3+1
* J'ai pas trouver d'image assez claire du coup cf cours


**********************

<h2> Circuit asynchrones</h2>

### Karnaugh

Avant de commencer rappel des <a href='http://daniel.robert9.pagesperso-orange.fr/Digit/images/Symboles_graphiques_des_differentes_fonctions_logiques.gif'> portes logiques </a>

Etape 1: Crée une table comme ceci:  
  ![Table Karnaugh](http://www.paturage.be/electro/inforauto/karnaugh/files/table-fonction-x.jpg)
Etape 2: Faire des paquets de 1 (de taille **2,4,8**)  
* Ex:

![Table Karnaugh 1](http://www.paturage.be/electro/inforauto/karnaugh/files/exemple1.jpg)
![Table Karnaugh](http://www.paturage.be/electro/inforauto/karnaugh/files/exemple2.jpg)
![Table Karnaugh](http://www.paturage.be/electro/inforauto/karnaugh/files/exemple3.jpg)

Etape 3: En deduire l'équation  
On écrit une valeur dans l'équation lorsque celle-ci ne change pas dans "le paquet" A=1 et \A(A barre)=0  
* Ex:

![Table Karnaugh zone](http://www.paturage.be/electro/inforauto/karnaugh/files/tableau-zones.jpg)

Etape 4: On realise le circuit   
> Un inverseur ne peut être appliqué qu'une fois par porte 

* Ex:

![schema elect](https://sti2d.ecolelamache.org/simul.png)
**********************

<h2> Bus </h2>

### Concretement on va me demander quoi ?

 Le bus est divisé en 3 parties: bus de données, bus d’adresse et bus instruction    
 Le bus de données permet d’envoyer une valeur à écrire ou revoir une valeur lue     
 Le bus d’adresse permet de dire l’adresse de la données    
 Le bus de controle permet d’envoyer des ordres sur le bus et des éléments de synchronisation    
 En réalité, souvent il n’y a pas un seul bus mais un ensemble complexe de bus interconnectés    

 Le temps d'un débit d'un bus:   
* Ex: 
* Un bus a une largeur de 64 bits; Il est cadencé à 100 MHz; Il faut 3 tops pour faire une lecture
*  D=64*(100 * 10^6)/3=6400 * 106/3 
*  D=2.33 Gbit/s 

> 64 la largeur, 100 * 10^6 la cadence; 3 le nombre de top 

************************