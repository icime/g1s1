1.	select distinct **JOB** from *emp*;
2.	select distinct **ENAME, JOB, SAL** from *em*p where **DEPTNO=20** and **SAL>2000**;
3.	select distinct **ENAME** from *emp* where **JOB='PRESIDENT'** or **JOB='MANAGER'**;
4.	select distinct **ENAME** from *emp* where **SAL>1200** and **SAL <1400**;
5.	select distinct **ENAME** from *emp* where **JOB='CLARK'** or **JOB='ANALYSTE'** or **JOB='SALESMAN'**;
6.	select distinct **ENAME** from *emp* where **Ename like 'M%'**;
7.	select distinct **ENAME** from *emp* where **Ename like '____N'**;
8.	select distinct **ENAME** from *emp* where **Ename not like '_____'**;
9.	select distinct **ENAME** from *emp* where **COMM is NULL**;
10.	select distinct **ENAME, SAL** from *emp* where **DEPTNO=30** order by **SAL DESC**;  
        select distinct **ENAME, SAL** from *emp* where **DEPTNO=30** order by **SAL ASC**;
11.	select distinct **ENAME, JOB, SAL** from *emp* where **DEPTNO=30** order by **ENAME ASC**;
select distinct **ENAME, JOB, SAL** from *emp* where **DEPTNO=30** order by **SAL DESC**;
12.	select distinct **ENAME, COMM** from *emp* where **COMM is not NULL** order by **COMM ASC**;
13.	 
14.	select distinct **DNAME** from *emp,dept* where **emp.ename='ALLEN'** and **dept.deptno=emp.deptno**;
15.	 
16.	 
17.	select **DEPTNO** from *dept* minus select **deptno** from *emp*;
18.	select **Dept.DNAME, emp**  from *emp, dept* where **loc='CHICAGO'**;
19.	Select distinct **E.ename, E.sal, M.ename, M.sal** from *emp E, emp M* where **E.sal > M.sal** and **E.job not in('MANAGER', 'PRESIDENT')** and **M.job = 'MANAGER'** and **E.deptno=M.deptno**;
20.	select **ENAME, SAL** from *EMP* where **SAL > ALL** (select **SAL** from *EMP* where **ENAME='JONES'**);
21.	select **GRADE, ENAME** from *EMP, SALGRADE* where **emp.sal>=salgrade.losal** and **emp.sal<=salgrade.hisal**;
22.	select **ENAME, JOB** from *EMP* where **JOB=ALL** (select **JOB** from *EMP* where **ENAME='JONES'**);
23.	select **ENAME, SAL, DEPTNO** from *EMP* where **SAL>ANY** (select **SAL** from *EMP* where **DEPTNO=30**);
24.	select **ENAME, SAL, DEPTNO** from *EMP* where **SAL>ALL** (select **SAL** from *EMP* where **DEPTNO=30**);
25.	Select **ENAME** from *emp* where **deptno=10** and **job=ANY** (select **JOB** from *emp* where **deptno=30** );
26.	select **ENAME, JOB, SAL** from *EMP* where **JOB=ALL** (select **JOB** from *EMP* where **ENAME='FORD'**);
27.	select distinct **ENAME, JOB, DEPTNO, SAL** from *EMP* where **JOB=ALL** (select **JOB** from *EMP* where **ENAME='JONES'**) or **SAL>=ALL** (select **SAL** from *EMP* where **ENAME='FORD'**) order by **SAL desc**;
28.	select **ENAME** from *EMP* where **DEPTNO=10** and **JOB=ANY** (select **emp.JOB** from *EMP, DEPT* where **DNAME='SALES'** and **emp.DEPTNO=dept.DEPTNO** );
29.	select distinct **ENAME** from *EMP,DEPT* where dept.LOC='CHICAGO' and JOB=ANY (select **emp.JOB** from *EMP, DEPT* where **ENAME='ALLEN'**);
30.	select **ENAME**  from *EMP* where **SAL>ALL** (select **avg(SAL)** from *emp* where **DEPTNO=DEPTNO**);
