1.	select **ENAME, JOB, SAL, COMM, SAL+**   
case  
    when **COMM is NULL then 0**  
    else **COMM**   
end  
as **Total_compensation** from *emp* where **deptno = 30**;   

2.	
* select **DEPTNO,POWER(DEPTNO, 2)**, **POWER(DEPTNO, 3)** from *DEPT*;  
* select **SAL, SAL/22, round(SAL/22,2)** from *EMP* where **DEPTNO=30**;  

3.	select **ROUND(SAL/22, 1)** as **Salaire_journalier, ROUND(SAL/22/8, 1)** as **Salaire_horaire** from *emp* where **deptno = 30**;
4.	select **DNAME||' - '||** ** LOC** from **DEPT**;
5.	select **ENAME**,   
case  
    when **JOB = 'CLERK'** then **1**  
    when **JOB = 'MANAGER'** then **3**   
    when JOB = 'PRESIDENT' then **5**  
    else **2**  
end  
as **job** from *emp*;  

6.	select **DEPTNO**,   
case  
    when **DEPTNO=10** then **JOB**  
    else **ENAME**
end
as **DESIGNATION** from *emp*;
7.	select **LPAD(DNAME, 5)** from *dept*;
8.	select **upper(ENAME)** as **MAJ**, **lower(ENAME)**as **MIN** from *emp*;
9.	select **INSTR(ENAME, 'R')** from *emp*;
10.	select **LENGTH(DNAME)** from *dept*;
11.	select **ENAME, SAL, LPAD('$', SAL/100, '$')** from *emp* order by **SAL ASC**;
12.	Select `*` from *emp* where **deptno=&x**;
13.	select **ENAME, JOB, To_Char(HIREDATE,'DD month YY')** from *emp* where **deptno=20**;
14.	select **HIREDATE, ENAME** from *emp* where **hiredate between '04/01/1961'** and **'15/04/1981'** order by **HIREDATE ASC**;
15.	select **MAX(SAL), MIN(SAL), MAX(SAL)-MIN(SAL)** from *emp*;
16.	select **MAX(LENGTH(DNAME))** from *dept*;
