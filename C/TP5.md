<pre><code> 
#include <stdio.h>
#include <stdlib.h>
#define Tab_Taille 100
char nb_char(char*,int);
void envers(char*, int);
void concat(char*, int,int, char*);
void retourne(char*, int);
void collage(char*,char*,int);

int main(){
    char tab_char1[Tab_Taille], tab_char2[Tab_Taille];
    printf("Veuillez saisir un mot:");
    scanf("%s",&tab_char1);
    printf("Veuillez saisir un autre mot:");
    scanf("%s",&tab_char2);

    printf("%d \n",nb_char(tab_char1, Tab_Taille));
    envers(tab_char1, nb_char(tab_char1, Tab_Taille));
    printf("\n");
    concat(tab_char2, nb_char(tab_char1, Tab_Taille),nb_char(tab_char2, Tab_Taille), tab_char1);
    retourne(tab_char1, nb_char(tab_char1, Tab_Taille));
    collage(tab_char1,tab_char2, nb_char(tab_char1, Tab_Taille));
}


char nb_char(char* tab_char, int Taille){
    int i=0;

    while (i<=Taille){
        if(tab_char[i]=='\0'){
            return i;
        }
        i++;
    }
}

void envers(char* tab_char, int Taille){
    int i;

    for(i=Taille-1;i>=0;i--){
        printf("%c",tab_char[i]);
    }
}
void concat(char* tab_char2,int Taille1, int Taille2 , char* tab_char1){
    int i;

    for(i=0;i<=Taille2;i++){
        tab_char1[Taille1+i]=tab_char2[i];
    }

    printf("%s \n",tab_char1);
}

void retourne(char* tab_char1,int Taille1){
    int i;
    char stock;

    for(i=0;i<Taille1/2;i++){
        stock=tab_char1[i];
        tab_char1[i]=tab_char1[Taille1-1-i];
        tab_char1[Taille1-1-i]=stock;
    }

    printf("%s \n",tab_char1);
}

void collage(char* tab_char1,char* tab_char2,int Taille){
    tab_char1=tab_char2;
    printf("%s \n",tab_char1);
} 
</code></pre>
