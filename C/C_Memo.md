# Memo C

`#include` déclaration des bibliothèques
* Ex : `#include <stdio.h>`

`#define` constante valeur : déclaration de constante, variables globales, Procédures, Programme principal

## Syntaxe programme :

```c
int main(void) {
    //programme
}
```

### Déclaration de variable

Syntaxe de déclaration de variables ;

*Ex : `int annee ; float x1, x2 ; int jour=5, mois=11 ; float note=17.6;`*

### Condition Principale

```c
for ( ; ; ) {
    // bloc
}
```

* Ex : afficher tous les caractères minuscules
```c
for (i=’a’ ; i<=’z’ ; i++) {
    printf(“%c “, i) ; printf(“\n“) ;
}
```
(afficher toutes les lettres de l’alphabet)

```c
while (/* Condition */) {
    // Bloc
}
```

* Ex :
```c
i= ‘a’;
while (i <= ’z’) {
    printf(“%c “, i);
    i++;
}
printf(“\n“);
```

```c
do {
    // Bloc
} while (/* Condition */);
```

```c
if (/* Condition */) {
    //Bloc
} else {
    // Bloc
};
```

```c
switch ( ) {
    case : ; ; break ;
    case : … ;
    default : ;
}
```

***

## Entrées - sorties

Saisie de variable : `scanf(““, &);`
* Ex: `scanf("%d",&x);`
Affichage : `printf("/n");`
* Ex : `printf("Phrase %d/n", x);` (`/n`  retour chariot (saut de ligne))

***

## Fonction


*Type du résultat* int, float… Nom_Fonction (Variable utilisée int,float…) {  /variables locales/ /instructions/ return ; }
* Ex :

```c
float calcul (float a, float b) {
    a = a/b;
    return a;
}
```

```c
void Nom_Fonction (Variable utilisée int,float…)  {
    // variables locales
    //instructions
}
```

* Ex :

```c
void bonjour() {
    printf("Bonjour");
}
```

***

## Tableau

`#define` **Taille_Tab NB**  Permet de modifier la longueur du tableau dans tous le programme

> La premiere valeur d'un tableau est 0 Tab[0] et la derniere Tab[i-1] (pour un tableau valant 10 Tab[9])

int main() {
Type de variable stocké dans le tableau <b>int,float,char Tableau [Taille_Tab]</b>; <em>Initialisation d’un tableau de la taille de Taille_Tab</em>

**Tableau[numeroDeLaCase]= x** ; *Dans une case du tableau permet de stocké une valeur x*
}

* Ex :

```c
#define Taille_Tab 4

void main (void){
    int Tableau[Taille_Tab];
    Tableau [0]=1;
    Tableau [1]=3;
    Tableau [2]=134;
    Tableau [3]=26;
}
```

### Dans une fonction :


```c
#define Taille_Tab 4

void affiche(int *Tableau, int Taille);

void main (void)
{
    int Tableau[Taille_Tab] = {10, 15, 3};
}

void affiche(int *Tableau, int Taille)
{
    int i;
    for (i = 0 ; i < tailleTableau ; i++) {
        printf("%d\n", tableau[i]);
    }
}
```
