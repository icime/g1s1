*Support de cours d'algo :*
 https://perso.liris.cnrs.fr/pierre-antoine.champin/enseignement/algo/cours/

*Recueil d'exercices:*
 https://perso.liris.cnrs.fr/pierre-antoine.champin/enseignement/algo/exercices

> N'hésitez pas à proposer vos corrections

