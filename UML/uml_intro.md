# intro

## les classes
- type personnalisé
- composé de plusieurs champs de types différents ou non
- contient des fonctions pour manipuler les type
- regroupement caractéristique communes à une chose
### exemple
une voiture qui a une marque, un modèle, une couleur, un n° d'immatriculation. On peut la faire démarrer, l'arrêter, la repeindre, etc...


## Les objets
un objet est associé a une classe.
La classe correspond à la vision globale alors que l'objet correspond à des valeurs précises des champs du type personnalisé. (c'est un peu le même principe que les variables et le type)
### exemple
Pour la classe voiture, on pourra trouver l'objet titine qui sera de la marque Renault, modèle Clio, de couleur rouge et d'immatriculation DR-234-GJ


## Vocabulaire
### classe
Nom donné au type personnalisé

### attribut
Nom donné aux champs de la classe

### méthodes
nom donné aux fonctions propres au classes. Elles permettent de manipuler les attributs d'une classe ou de spécifier le comportement propre aux objets de la classe