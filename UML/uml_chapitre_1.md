# Chapitre 1

## Définitions
- **diagramme de classe:** Partie statique du système, contient des classes et des associations
- **diagramme d'objet:** Sert à montrer un contexte ou à faciliter la compréhension des objet complexes

## Les classes
*(image diagramme de classe)*


## Les associations
### simple
Les classes sont représentées sans les méthodes

### simple avec rôle
Les classes sont représentées juste avec les noms

### réflexive
La classe est associée avec elle même

### classe association
Représente un attribut qui dépend de deux classes différentes

### l'arité des associations
Représente le nombres de classes d'une association

### multiplicité
Montre combien d'objet de la classe considérés peuvent être associés avec l'autre classe.
#### exemple
- 1...1 ==> 1
- 0...1
- m...n
- 0...* ==> *
- 1...*


## L'agrégation
### simple
- relie une classe agrégat à une classe agrégé
- À chaque objet de la classe agrégé on peut associé un ou plusieurs objets de la classe agrégat
- C'est une association qui lie fortemet les classes
Une action sur une classe implique une action sur une autre classe ou alors les objets d'une classe sont subordonnés aux objets d'une autre classe.

### composition
Cas particulier de l'agrégation. La relation des deux classes est encore plus forte, la destruction de l'agrégat entraine la destruction de l'agrégé.