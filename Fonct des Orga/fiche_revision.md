# Fonction Orga

### Type d’organisation :
- Entreprise
- Administration public
- Administration privée

### L’entreprise une cellule sociale,

* Les discriminations:
    * raciales
    * religieuses
    * sexuelles

* L’humain est un outil de compétitivité
    * Force de travail
    * Motivation
Un employé a un statut une compétence et une qualification

### Definition

**SWOT** : Diagnostique stratégique d’une organisation

**PESTEL**:Politique Economique Sociologique Technique Ecologique Legale

Depuis les *années 70*, l’impact de **l’environement** sur le l’entreprise s’intensifie  
* de la conjoncture économique  
* du champs concurentiel des perspectives de localisation des activités liées a la mondialisation  
* de la tecnologique a un tythme plus soutenu  
* de la societé  

> On parle de tuburlence de l’environement.  

3752500 entreprises en 2013  
    68.4 % pas de salariés  
    26.2 % entre 1 et 9  
    4.5 % entre 10 et 49  
    0.7 % entre 50 et 199  
    0.1 % entre 200 et 499  
    … entre 500 et 1999  
    … entre 2000 et plus  

Le maillage
1. TIC =
2. w=

### Devoir sur les documents avec les PME et multinational et les classements des entreprises

Les societés françaises sont detenues par des societés étrangères (par les actions)

1. Organisation :
	* administration privée
	* administration public
    * entreprise

EBE sert a financer les actionnaires, cash flow, salariés

Sous traitence en cascade , chaques produits et chaques elements est fabriqué par une autre entreprise  
Sous traitente et le produit final est monter dans l’entreprise de la boite finale qui donnera son nom

**RSE**: Responsabilité social des entreprise

Intégration des handicapés et minorités


### RSE
1. Raison économique
	* L’entreprise veut tirer les cordes
	* Outils marketing
	* Devenir précurseur sur les futures normes à venir
	* Innover et maitriser de la compétitivité hors-coût
	* Attractivité des investisseurs *(fonds de pensions)*
	* Attente des agences de dotation et AAI *(Autorité administrative indépendante)* et AMF *(Autorité des marché financer)*
	* Productivité accrue
2. Raison sociologique
	* Le consommateur veut la transparence *(TPP, transparence pure et parfaite)* face au scandale sanitaire
	* Attractivité des profils de haut niveau, *(ingénieur qualité, R&D)*
	* Salariés mieux formés donc investis dans l’entreprise
	* Conditions de travail améliorées, donc bien être amélioré 

### Exemple d'Analyse swot 

![Swot](https://www.manager-go.com/assets/Uploads/swot-personnel.png)
![Swot Coca-Cola](https://www.marketing-etudiant.fr/wp-content/uploads/2017/02/swot-coca-cola.jpg)
